<?php
header('Content-Type: text/html; charset=utf-8'); 
require "../../head.php";
 begin("search");
 ?>
		<div class='col-md-4 col-md-offset-1'>
			<h4>SEARCH ITEM</h4>
			<form data-toggle="validator" role="form" id='componentInfo' method="POST" action="../main/ComponentInfo.php">
				<div class="form-group">
					<label for="exampleInputEmail1">Item</label>
					<input class="form-control" type="text" placeholder="Barcode" name="barCode" required>
				</div>
				<input class="btn btn-default" type="submit" value="Search">
			</form>
		</div>
		<div class='col-md-4 col-md-offset-1'>
			<h4>SEARCH TEAM</h4>
			<form role="form" id='teamInfo' data-toggle="validator" method="POST" action="../main/TeamInfo.php">
				<div class="form-group">
					<label for="exampleInputEmail1">Team</label>
					<input class="form-control" type="text" placeholder="Barcode" name="barCode" required>
				</div>
				<input class="btn btn-default" type="submit" value="Search">
			</form>
		</div>
	</body>
</html>