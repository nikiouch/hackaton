<?php
header('Content-Type: text/html; charset=utf-8'); 
require "../../head.php";
 begin("team");
 ?>

		<div class="panel-group col-md-5 col-md-offset-1" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		      	<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		        	View all teams
		        </a>
		      </h4>
		    </div>
		    <div id="collapseOne" class="panel-collapse collapse">
		      <div class="panel-body">
		        <?php
					$table = "<table class=\"table\"><thead><tr><th>#</th><th>Name</th></tr></thead>";
		        	$xml = simplexml_load_file("../../resources/team.xml");
					$xmlComponent = simplexml_load_file('../../resources/component.xml');
					$i = 1;
					foreach($xml->team as $team)
					{
						
						if(isset($team->items->item))
						{	
							$tr = "<tr class=\"info\"><td>".$i."</td>";
						}
						else
						{
							$tr = "<tr ><td>".$i."</td>";
						}
							$tr = $tr."<td><div class=\"panel-group\" id=\"accordion\">
							  <div class=\"panel panel-default\">
							    <div class=\"panel-heading\">
							      <h4 class=\"panel-title\">
							              <a data-toggle=\"collapse\"  href=\"#collapse".$i."\">
							                ".$team->name."
							              </a>
							            </h4>
							    </div>
							    <div id=\"collapse".$i."\" class=\"panel-collapse collapse\">
							      <div class=\"panel-body\">";
							$tab = "<table class='table table-striped'><thead><tr><th style=\"width:10px;\">#</th><th>Name</th><th>Quantity</th></tr></thead>";
							$j = 1;
							if(isset($team->items->item)){
							foreach($team->items->item as $item)
							{
								$node = $xmlComponent->xpath('//component[@id="'.$item['id'].'"]');
								$tab = $tab."<tr><td width=\"10px\">".$j."</td><td>".$node[0]->name."</td><td>".$item->count."</td></tr>";
								
							}
							}else{
								$tab = $tab."<tr><td>-</td><td>Empty</td><td>-</td></tr>";	
							}
									$tr = $tr.$tab."</table></div>
								    </div>
								  </div>	  
								</div>";
						
						$table = $table.$tr."</tr>";
						$i++;
					}
					echo($table."</table>");
		        ?>
		      </div>
		    </div>
		  </div>	
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		      	<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
		        	View teams with items
 		        </a>
		      </h4>
		    </div>
		    <div id="collapseTwo" class="panel-collapse collapse">
		      <div class="panel-body">
		        <?php
					$table = "<table class=\"table\"><thead><tr><th>#</th><th>Name</th></tr></thead>";
		        	$xml = simplexml_load_file("../../resources/team.xml");
					$xmlComponent = simplexml_load_file('../../resources/component.xml');
					$i = 1;
					foreach($xml->team as $team)
					{
						if(isset($team->items->item))
						{	
							$tr = "<tr class=\"info\"><td>".$i."</td>";
							$tr = $tr."<td><div class=\"panel-group\" id=\"accordion\">
							  <div class=\"panel panel-default\">
							    <div class=\"panel-heading\">
							      <h4 class=\"panel-title\">
							              <a data-toggle=\"collapse\"  href=\"#collapse".$i."\">
							                ".$team->name."
							              </a>
							            </h4>
							    </div>
							    <div id=\"collapse".$i."\" class=\"panel-collapse collapse\">
							      <div class=\"panel-body\">";
							$tab = "<table class='table table-striped'><thead><tr><th style=\"width:10px;\">#</th><th>Name</th><th>Quantity</th></tr></thead>";
							$j = 1;
						}
						if(isset($team->items->item)){
							foreach($team->items->item as $item)
							{
								$node = $xmlComponent->xpath('//component[@id="'.$item['id'].'"]');
								$tab = $tab."<tr><td width=\"10px\">".$j."</td><td>".$node[0]->name."</td><td>".$item->count."</td></tr>";
								
							}
							$tr = $tr.$tab."</table></div>
						    			</div>
						  			</div>	  
								</div>";
							$table = $table.$tr."</tr>";
							$i++;
						}
					}
					echo($table."</table>");
		        ?>
		      </div>
		    </div>
		  </div>	    
		</div>
		<div class='col-md-4 col-md-offset-1'>
			<h4>ADD TEAM</h4>
			<form role="form" data-toggle="validator" id='addTeam' method="POST" action="../main/AddTeam.php">
				<div class="form-group">
					<label for="exampleInputEmail1">Name</label>
					<input class="form-control" type="text" id='teamBarCode' placeholder="The name of the team" name="name" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Barcode</label>
					<input class="form-control" type="text" id='barCode' placeholder="Barcode" name="barCode" required>
				</div>
				<input class="btn btn-default" type="submit" value="Done">
			</form>
		</div>
	</body>
</html>