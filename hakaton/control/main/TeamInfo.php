<?php
header('Content-Type: text/html; charset=utf-8'); 
if(isset($_GET['barCode'])){
	$barCode=$_GET['barCode'];
}else{
	$barCode=$_POST['barCode'];
}
if (file_exists('../../resources/component.xml')) {
	$number=1;
    $xml = simplexml_load_file('../../resources/team.xml');
    $xmlComponent = simplexml_load_file('../../resources/component.xml');
    foreach ($xml->team as $team) {
    	if((string)$barCode==(string)$team['id'])
    	{
    		$items=$team->items;
    	}
    }
    ?>
    <?php
require "../../head.php";
 begin("search");
 ?>
			<h4></h4><br>
			<div class="container">
			<div class="row">
				<div class="col-md-1">
					<form action="../main/Search.php"><input class="btn btn-default" type="submit" value="Back" >
					</form>
				</div>
			</div>
			</div>
			<br>
			<div class="container">
				<div class="col-xs-7">
				<div class="row">
					<h4>
					<?php 
					$node = $xml->xpath('//team[@id="'.$barCode.'"]');  
					echo $node[0]->name;
					?>
					</h4>
					<table class="table table-striped"> 
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th>
								Count
							</th>
							<th>
								Delete
							</th>
						</tr>
						<?php
							if(isset($items)){
							foreach($items->item as $item){
								?>
								<tr>
									<td>
										<?php echo $number; ?>
									</td>	
									<td>
										<?php 
										$node = $xmlComponent->xpath('//component[@id="'.$item['id'].'"]');
										echo $node[0]->name; ?>
									</td>
									<td>
										<?php echo $item->count; ?>
									</td>
									<td>
										<form action="../main/DeleteComponentFromTeam.php">
										<?php 
											echo('<input type="hidden" name="Team" value="'.$barCode.'" />');
											echo('<input type="hidden" name="Item" value="'.$item['id'].'" />'); 
										?>
										<input type="submit" class="btn btn-default" value="Delete" ></form>
									</td>
								</tr>
								<?php
								$number=(int)$number+1;
							}
						}
						?>
					</table>
				</div>
				</div>
			</div>
		</body>
	</html>
    <?php
	echo('<div class="container"><div class="row"><div class="col-md-2"><form action="../main/Search.php"><input class="btn btn-default" type="submit" value="Back" ></form></div></div></div>');
} else {
    exit('Не удалось открыть файл resources/team.xml');
}
?>
