<?php
header('Content-Type: text/html; charset=utf-8');
//Файл test.xml содержит XML-документ с корневым элементом 
//и, по крайней мере, элемент /[root]/title.
if (file_exists('../../resources/team.xml')) {
    $xml = simplexml_load_file('../../resources/team.xml');
    foreach ($xml->team as $a) {
    	if((string)$_POST['barCode']==(string)$a['id'])
    	{
    		header('Location: ../main/Team.php');
    		die;
    	}
    }
    $name=$_POST['name'];
    $barCode=$_POST['barCode'];
    $team=$xml->addChild('team');
    $team->addAttribute('id',$barCode);
    $team->addChild('name',$name);
    $items=$team->addChild('items');
    file_put_contents('../../resources/team.xml',$xml->asXML());
    header('Location: ../main/Team.php');
} else {
    exit('Не удалось открыть файл resources/team.xml');
}
?>