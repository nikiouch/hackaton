<?php
header('Content-Type: text/html; charset=utf-8'); 
require "../../head.php";
 begin("settings");
 bodySettings("clear","Erase Database");
 ?>			
 				<div>
	 				<form method="POST" action="../settings/EraseDatabase.php">
	 					<div class="form-group">
							<input type="checkbox" name="eraseTeam" value="Yes"> Erase Team <br>
						</div>
	 					<div class="form-group">
			 				<input type="checkbox" name="eraseItem" value="Yes"> Erase Item <br>
						</div>
	 					<div class="form-group">
			 				<input type="checkbox" name="eraseItemFromTeam" value="Yes"> Erase Items From Team <br>
						</div>
			 			<input type="submit" class="btn btn-default" value="Erase">
		 			</form>
	 			</div>
 			</div>
      	</div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../../assets/js/docs.min.js"></script>
  </body>
</html>