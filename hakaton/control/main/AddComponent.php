<?php
header('Content-Type: text/html; charset=utf-8');
$message1="?message=Success!";
$message2="";
//Файл test.xml содержит XML-документ с корневым элементом 
//и, по крайней мере, элемент /[root]/title.
if (file_exists('../../resources/component.xml')) {
    $xml = simplexml_load_file('../../resources/component.xml');
    foreach ($xml->component as $a) {
    	if((string)$_POST['barCode']==(string)$a['id'])
    	{

    		$a->count+=(int)$_POST['count'];
    		file_put_contents('../../resources/component.xml',$xml->asXML());
            $message2="&message2=This item already exist and the quantity was incremented from <mark>".((int)$a->count-(int)$_POST['count'])."</mark> to <mark>".((int)$a->count)."</mark>.";
    		header('Location: ../main/Item.php'.$message1.$message2);
    		die;
    	}
    }
    $message2="&message2=Item was added.";
    $name=$_POST['name'];
    $count=$_POST['count'];
    $barCode=$_POST['barCode'];
    $component=$xml->addChild('component');
    $component->addAttribute('id',$barCode);
    $component->addChild('name',$name);
    $component->addChild('count',$count);
    $component->addChild('taken','0');
    file_put_contents('../../resources/component.xml',$xml->asXML());
    header('Location: ../main/Item.php'.$message1.$message2);
} else {
    header('Location: ../main/Item.php?message=Error!');
}
?>