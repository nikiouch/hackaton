<?php
header('Content-Type: text/html; charset=utf-8'); 
require "../../head.php";
 begin("main");
 ?>
		<?php 
		if (isset($_GET['message'])){
			$success='Success!';
			if((String)$_GET['message']!=$success){
				echo('<div class="alert alert-danger" role="alert">'.$_GET['message'].' '.$_GET['message2'].'</div>');
			}else{
				echo('<div class="alert alert-success" role="alert">'.$_GET['message'].' '.$_GET['message2'].'</div></div>');	
			}
		} 
		?>
		<div class='col-md-4 col-md-offset-1'>
			<h4>ADD ITEM TO TEAM</h4>
			<form role="form" id='addComponentToTeam' name='addComponentToTeam' data-toggle="validator" method="POST" action="../main/AddComponentToTeam.php">
				<div class="form-group">
					<label for="exampleInputEmail1">Team</label>
					<input class="form-control" type="text" id='teamBarCode' placeholder="Barcode" name="teamBarCode" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Item</label>
					<input class="form-control" type="text" id='itemBarCode' placeholder="Barcode" name="itemBarCode" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Quantity</label>
					<input class="form-control" type="text" id='count' placeholder="Quantity" name="count" required>
				</div>
				<input class="btn btn-default" type="submit" value="Done">
			</form>
		</div>
		<div class='col-md-4 col-md-offset-1'>
			<h4>DELETE ITEM FROM TEAM</h4>
			<form role="form" id='deleteComponentFromTeam' name='deleteComponentFromTeam' data-toggle="validator" method="POST" action="../main/DeleteComponentFromTeam.php">
				<div class="form-group">
					<label for="exampleInputEmail1">Team</label>
					<input class="form-control" type="text" id='teamBarCode' placeholder="Barcode" name="teamBarCode" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Item</label>
					<input class="form-control" type="text" id='itemBarCode' placeholder="Barcode" name="itemBarCode" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Quantity</label>
					<input class="form-control" type="text" id='count' placeholder="Quantity" name="deleteCount" required>
				</div>
				<input class="btn btn-default" type="submit" value="Done">
			</form>
		</div>
	</body>
</html>