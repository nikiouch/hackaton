<?php
header('Content-Type: text/html; charset=utf-8');
$count=0;
if (file_exists('../../resources/component.xml')) {
	if(isset($_GET['Team'])){
		$itemBarCode=$_GET['Item'];
		$teamBarCode=$_GET['Team'];
		$url='../main/TeamInfo.php?barCode='.$teamBarCode;
	}else{
		$itemBarCode=$_POST['itemBarCode'];
		$teamBarCode=$_POST['teamBarCode'];
		$deleteCount=$_POST['deleteCount'];
		$url1='?message=Success!';
	}
	$error=0;
	$count=0;
	$teamExist=0;
	$itemExist=0;
	$url2='';
    $xmlComponent = simplexml_load_file('../../resources/component.xml');
    $xmlTeam = simplexml_load_file('../../resources/team.xml');
    foreach($xmlTeam->team as $team){
    	if((String)$team['id']==(String)$teamBarCode){
    		$teamExist=1;
    	}
    }
    if((int)$teamExist==1){
		foreach($xmlTeam->team as $team){
			if((String)$team['id']==(String)$teamBarCode){
				foreach($team->items->item as $item){
					if((String)$item['id']==(String)$itemBarCode){
						$itemExist=1;
						if((int)$deleteCount==(int)$item->count){
							$count=$item->count;
							$dom=dom_import_simplexml($item);
	        				$dom->parentNode->removeChild($dom);
	        				$url2='&message2=Item was deleted!';
        				}else{
        					if((int)$deleteCount < (int)$item->count){
        						$count=$deleteCount;
        						$item->count=(int)$item->count-(int)$deleteCount;
        						$url2='&message2=Was deleted <mark>'.$count.'</mark> items!';
        					}else{
     							$url1='?message=Item couldn\'t be deleted! Was taken <mark>'.$deleteCount.'</mark> items';   						
        					}
        				}
					}
				}
			}
    	}
    	if((int)$itemExist==0){
    		$url='../main/index.php?message=Item doesn\'t exist!';
   		}
   		if((int)$error==0)
	   		foreach($xmlComponent->component as $component){
		    	if((String)$component['id']==(String)$itemBarCode){
		    		if((int)$component->taken>=$count){
		    			$component->taken=(int)$component->taken-(int)$deleteCount;
		    			file_put_contents('../../resources/component.xml',$xmlComponent->asXML());
		    		}
		    	}
		    }
	}else{
		$url='../main/index.php?message=Team doesn\'t exist!';
	}
    file_put_contents('../../resources/team.xml',$xmlTeam->asXML());
} else {
    exit('Не удалось открыть файл resources/component.xml');
}
header('Location:../main/index.php'.$url1.$url2);
?>