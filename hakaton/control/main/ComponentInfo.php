<?php
header('Content-Type: text/html; charset=utf-8'); 
//Файл test.xml содержит XML-документ с корневым элементом 
//и, по крайней мере, элемент /[root]/title.
if (file_exists('../../resources/component.xml')) {
    $xml = simplexml_load_file('../../resources/component.xml');
    foreach ($xml->component as $a) {
    	if((string)$_POST['barCode']==(string)$a['id'])
    	{
    		$name=$a->name;
    		$count=$a->count;
    		$taken=$a->taken;
    	}
    }
?>
<?php
require "../../head.php";
 begin("search");
 ?>
            <h4></h4><br>
            <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <form action="../main/Search.php"><input class="btn btn-default" type="submit" value="Back" >
                    </form>
                </div>
            </div>
            </div>
            <br>
            <div class="container">
                <div class="col-xs-7">
                <div class="row">
                    <?php if(isset($name)){ ?>
                    <table class="table table-striped"> 
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Name
                            </th>
                            <th>
                                Count
                            </th>
                            <th>
                                Taken
                            </th>
                        </tr>
                        </thead>
                        <tr>
                            <td>
                            </td>   
                            <td>
                                <?php 
                                echo $name; ?>
                            </td>
                            <td>
                                <?php echo $count; ?>
                            </td>
                            <td>
                                <?php echo $taken; ?>
                            </td>
                        </tr>
                    </table>
                    <?php }else{echo("No matching for ".$_POST['barCode']);} ?>
                </div>
                </div>
            </div>
        </body>
    </html>
<?php } else {
    exit('Не удалось открыть файл resources/team.xml');
}
?>
