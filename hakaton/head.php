
<?php
function begin($location){
?>	
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
 		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link href="../../bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  		<script src="../../resources/jquery-2.1.3.min.js"></script>
  		<script src="../../bootstrap/dist/js/bootstrap.min.js"></script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="../../bootstrap/dashboard.css" rel="stylesheet">
		<title>Hakaton 2014</title>
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
		  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="../main/index.php">Control system</a>
		    </div>
		    <div>
			    <ul class="nav navbar-nav">
			    <?
			    switch ((string)$location) {
			    	case 'team':
					    echo '<li class="active"><a href="../main/Team.php">Team</a></li>
					    	<li><a href="../main/Item.php">Item</a></li>';
			    		break;
			    	case 'item':
				        echo '<li><a href="../main/Team.php">Team</a></li>
					    	<li class="active"><a href="../main/Item.php">Item</a></li>';
			    		break;
			    	default:
			    		echo '<li><a href="../main/Team.php">Team</a></li>
					    	<li><a href="../main/Item.php">Item</a></li>';
			    		break;
			    }?>
			    </ul>
		    </div>
		    <div>
		    	<ul class='nav navbar-nav navbar-right'>
		    	<?php
				switch ((string)$location) {
			    	case 'search':
			    		echo '<li class="active"><a href="../main/Search.php">Search</a></li>
					    	<li><a href="../settings/">Settings</a></li>';
			    		break;
			    	case 'settings':
				        echo '<li><a href="S../main/earch.php">Search</a></li>
					    	<li class="active"><a href="../settings/">Settings</a></li>';
			    		break;
			    	
			    	default:
			    		echo '<li><a href="../main/Search.php">Search</a></li>
					    	<li><a href="../settings/">Settings</a></li>';
			    		break;
			    }
			    ?>
		    	</ul>
		    </div>
		  </div>
		</nav>
		<?
}


function bodySettings($location,$pageHeader){?>
		<div class="container-fluid">
      		<div class="row">
        		<div class="col-sm-3 col-md-2 sidebar">
          			<ul class="nav nav-sidebar">
           				<li class="active"><a href="../settings/">Erase Database</a></li>
            			<li><a href="#statistics">Statistics</a></li>
            		</ul>
        		</div>
        		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          			<h1 class="page-header"><? echo $pageHeader;?></h1>
        		
		<?php
}
?>