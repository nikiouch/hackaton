<?php
header('Content-Type: text/html; charset=utf-8'); 
require "../../head.php";
 begin("item");
 ?>
 <?php 
		if (isset($_GET['message'])){
			$success='Success!';
			if((String)$_GET['message']!=$success){
				echo('<div class="alert alert-danger" role="alert">'.$_GET['message'].' '.$_GET['message2'].'</div>');
			}else{
				echo('<div class="alert alert-success" role="alert">'.$_GET['message'].' '.$_GET['message2'].'</div></div>');	
			}
		} 
		?>
		
		<div class="panel-group col-md-5 col-md-offset-1" id="accordion">
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		      	<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
		        	View all Items
		        </a>
		      </h4>
		    </div>
		 <div id="collapseOne" class="panel-collapse collapse">
		      <div class="panel-body">
		        <?php
					$table = "<table class=\"table table-striped\"><thead><tr><th>#</th><th width='auto'>Name</th><th width='auto'>Quantity</th><th width='auto'>Taken</th></tr></thead>";
		        	$xml = simplexml_load_file("../../resources/component.xml");
					$i = 1;
					foreach($xml->component as $item)
					{
						if((int)$item->count==(int)$item->taken){
							$tr = "<tr class=\"danger\">";
						}else{
							if((int)$item->taken>0){
								$tr = "<tr class=\"warning\">";
							}else{
								$tr = "<tr class=\"success\">";
							}	
						}
						$tr=$tr."<td>".$i."</td><td>".$item->name."</td><td>".$item->count."</td><td>".$item->taken."</td>";
							$table = $table.$tr."</tr>";
							$i++;
					}
					echo($table."</table>");
		        ?>
		      </div>
		    </div>
		  </div>
		  <div class="panel panel-default">
		    <div class="panel-heading">
		      <h4 class="panel-title">
		      	<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
		        	View unreturned items
		        </a>
		      </h4>
		    </div>
		 <div id="collapseTwo" class="panel-collapse collapse">
		      <div class="panel-body">
		        <?php
					$table = "<table class=\"table table-striped\"><thead><tr><th>#</th><th width='auto'>Name</th><th width='auto'>Quantity</th><th width='auto'>Taken</th></tr></thead>";
		        	$xml = simplexml_load_file("../../resources/component.xml");
					$i = 1;
					foreach($xml->component as $item)
					{
						if((int)$item->taken>0){
							$tr = "<tr class=\"danger\"><td>".$i."</td><td>".$item->name."</td><td>".$item->count."</td><td>".$item->taken."</td>";
							$table = $table.$tr."</tr>";
							$i++;
						}
					}
					echo($table."</table>");
		        ?>
		      </div>
		    </div>
		  </div>	  	  
		</div>
		
		<div class='col-md-4 col-md-offset-1'>
			<h4>ADD ITEM</h4>
			<form data-toggle="validator" role="form" id='addToDatabase' name='addToDatabase' onsubmit="return validate()" method="POST" action="../main/AddComponent.php">
				<div class="form-group">
					<label for="exampleInputEmail1">Name</label>
					<input class="form-control" type="text" id='teamBarCode' placeholder="The name of the item" name="name" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Quantity</label>
					<input class="form-control" type="text" id='itemBarCode' placeholder="The number of items" name="count" required>
				</div>
				<div class="form-group">
					<label for="exampleInputEmail1">Barcode</label>
					<input class="form-control" type="text" id='itemBarCode' placeholder="The barcode of the item" name="barCode" required>
				</div>
				<input class="btn btn-default" type="submit" value="Done">
			</form>
		</div>
	</body>
</html>