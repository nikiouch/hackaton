<?php
header('Content-Type: text/html; charset=utf-8');
if (file_exists('../../resources/component.xml')) {
//VAR
	$count=0;
    $url="index.php?message=Success!";
	$teamExist=0;
    $itemExist=0;
	$itemAlreadyExist=0;
    $quantity=$_POST['count'];
	$itemBarCode=$_POST['itemBarCode'];
	$teamBarCode=$_POST['teamBarCode'];
    $xmlComponent = simplexml_load_file('../../resources/component.xml');
    $xmlTeam = simplexml_load_file('../../resources/team.xml');
//VAR
    foreach($xmlTeam->team as $team){
    	if((String)$team['id']==(String)$teamBarCode){
    		$teamExist=1;
    	}
    }
    if((int)$teamExist==1){
	    foreach($xmlComponent->component as $component){
	    	if((String)$component['id']==(String)$itemBarCode){
                $itemExist=1;
	    		$count=(int)$component->count-(int)$component->taken;
	    		if((int)$count>=(int)$quantity && (int)$count>0){
	    			$component->taken=(int)$component->taken+$quantity;
                }else{
                    $url="../main/index.php?message=Not enough items. We have only <mark>".$count."</mark> available now.";
                    header('Location:'.$url);
                    die;
                }
	    		file_put_contents('../../resources/component.xml',$xmlComponent->asXML());
	    	}
	    }
        if((int)$itemExist==0){
            $url="../main/index.php?message=Item doesn't exist!";
            header('Location:'.$url);
            die;
        }
		foreach($xmlTeam->team as $team){
			if((String)$team['id']==(String)$teamBarCode){
				foreach($team->items->item as $item){
					if((String)$item['id']==(String)$itemBarCode){
						$item->count=(int)$item->count+$quantity;
                        $itemAlreadyExist=1;
					}
				}
				if((int)$itemAlreadyExist==0){
					$item=$team->items->addChild('item');
					$item->addAttribute('id',$itemBarCode);
					$item->addChild('count',$quantity);	
				}
			}
    	}
	}else{
        $url="../main/index.php?message=Team doesn't exist!";   
    }
    
    
    file_put_contents('../../resources/team.xml',$xmlTeam->asXML());
    header('Location:'.$url);
} else {
    exit('Не удалось открыть файл resources/component.xml');
}
?>