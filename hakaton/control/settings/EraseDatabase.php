<?php
header('Content-Type: text/html; charset=utf-8'); 
if (file_exists('../../resources/component.xml')) {
	$itemBarCode=$_POST['itemBarCode'];
	$deleteCount=$_POST['deleteCount'];
	$url1='?message=Success!';
	$error=0;
	$count=0;
	$url2='';
	$success=copy ('../../resources/component.xml', '../../copy/component '.date("d.m H_i_s").'.xml');
	$success=copy ('../../resources/team.xml','../../copy/team '.date("d.m H_i_s").'.xml');
    $xmlComponent = simplexml_load_file('../../resources/component.xml');
    $xmlTeam = simplexml_load_file('../../resources/team.xml');
    if($_POST['eraseItemFromTeam']=='Yes'){
		foreach($xmlTeam->team as $team){
			unset($team->items->item);
		}
		if((int)$error==0){
			foreach($xmlComponent->component as $component){
	    		$component->taken=0;	    	
	    	}
	    }
	}
	if($_POST['eraseTeam']=='Yes'){
		unset($xmlTeam->team);
		if((int)$error==0){
			foreach($xmlComponent->component as $component){
	    		$component->taken=0;	    	
	    	}
	    }
	}
	if($_POST['eraseItem']=='Yes'){
		unset($xmlComponent->component);
		foreach($xmlTeam->team as $team){
			unset($team->items->item);
		}
	}

	file_put_contents('../../resources/component.xml',$xmlComponent->asXML());
    file_put_contents('../../resources/team.xml',$xmlTeam->asXML());
} else {
    exit('Не удалось открыть файл resources/component.xml');
}
header('Location:../settings/');
?>